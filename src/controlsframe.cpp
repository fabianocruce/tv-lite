#include "controlsframe.h"
#include "main.h"

ControlsFrame::ControlsFrame(wxWindow *parent):ControlsFrameBase(parent)
{
}

ControlsFrame::~ControlsFrame()
{
}

void ControlsFrame::OnMoveOver( wxMouseEvent& event )
{
   MainFrame* mainFrame = (MainFrame*)wxGetApp().GetMainFrame();
   wxTimer* mouseTimer = mainFrame->GetMouseTimer();
   
   if (!mainFrame->isFullScreen()) return;
   if (mouseTimer->IsRunning()) 
   { 
       mouseTimer->Stop();  
   }
}