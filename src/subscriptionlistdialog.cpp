#include <wx/wx.h>
#include "subscription.h"
#include "subscriptiondialog.h"
#include "subscriptionlistdialog.h"
#include "xtreamdlg.h"
#include "dlprogress.h"
#include "debug.h"

using namespace tvlite;

CSubscriptionListDialog::CSubscriptionListDialog(wxWindow* parent):CSubscriptionListDialogBase(parent)
{
   int width, height;
   m_SubscriptionDataList->GetSize(&width, &height);
   m_SubscriptionDataList->AppendTextColumn( _("Name"), wxDATAVIEW_CELL_INERT, width/3 );
   m_SubscriptionDataList->AppendTextColumn( _("URL"), wxDATAVIEW_CELL_INERT,width*2/3 );
   m_subscriptionList = ((MainFrame*)wxGetApp().GetTopWindow())->GetSubscriptionList();
   m_close->SetDefault();
   Repopulate();
}

CSubscriptionListDialog::~CSubscriptionListDialog()
{
   
}

void CSubscriptionListDialog::OnNewClicked( wxCommandEvent& event )
{
  CBusyGuard guard(this);
  CSubscription* newSub = wxGetApp().GetMainFrame()->GetTempSubscription();
  if (!newSub)
  {
     newSub = new CSubscription("");
     wxGetApp().GetMainFrame()->SetTempSubscription(newSub);
  }   
  if (guard.Allows())
   {
      CSubscriptionDialog *newDialog = new CSubscriptionDialog(this);
      int rc = newDialog->ShowURLOnly();
      if (rc == wxID_OK)
      {
         CSubscriptionInfo *newSubInfo = newSub->GetSubscriptionInfo();
         newDialog->GetData(newSubInfo);
         if (CSubscription::FindCachedData(newSubInfo->url) != "")
         {
            wxMessageBox(_("Found another subscription with the same URL"), _("Error"), wxOK | wxICON_ERROR, NULL);
         }
         else
         {
            newSub->SetURI(newSubInfo->url);
            if (UpdateSubscription(newSub) != E_DLP_OK)
            {
               wxMessageBox(_("Could not set up the selected subscription"), _("Error"), wxOK | wxICON_ERROR, NULL);
            }
            else
            {
               ((MainFrame*)wxGetApp().GetMainFrame())->RefreshInterface(1, newSubInfo->url);
               Repopulate(newSubInfo->url);
               wxMessageBox(_("Subscription set up"), _("New subscription"), wxOK | wxICON_INFORMATION, NULL);
            }
         }
      }
      newDialog->Destroy(); 
      delete newDialog;
   }
  
} 

void CSubscriptionListDialog::OnEditClicked( wxCommandEvent& event ) 
{
   CBusyGuard guard(this);
   int index = m_SubscriptionDataList->GetSelectedRow();
   if (index == wxNOT_FOUND)
   {
      return;
   }
   if (guard.Allows())
   {
      CSubscriptionDialog *editDialog = new CSubscriptionDialog(this);
      CSubscription* currentsubscription = (*m_subscriptionList)[index];
      CSubscriptionInfo *currentsubInfo = (*m_subscriptionList)[index]->GetSubscriptionInfo();
      wxString oldName = currentsubInfo->name;
      wxString oldURL  = currentsubInfo->url;
      editDialog->SetData(currentsubInfo);
      int rc = editDialog->ShowModal();
      if (rc == wxID_OK)
      {
         editDialog->GetData(currentsubInfo);
         rc = E_DLP_OK;
         if (oldURL != currentsubInfo->url)
         {
            currentsubscription->SetURI(currentsubInfo->url);
            rc = UpdateSubscription(currentsubscription);
            if (rc != E_DLP_OK)
            {
               wxMessageBox(_("Could not modify the selected subscription"), _("Error"), wxOK | wxICON_ERROR, NULL);
               //restore url
               currentsubInfo->url = oldURL;
               currentsubscription->SetURI(currentsubInfo->url);
            }
            else
            {
               //find subscription with the old url and delete it
               wxString deleteFileName = CSubscription::FindCachedData(oldURL);
               if (deleteFileName != "")
               {
                  DBG_INFO("Deleting old file");
                  wxRemoveFile(deleteFileName);
               }
            }
         }
         if (rc == E_DLP_OK)
         {   
            wxString currentFileName = CSubscription::FindCachedData(currentsubInfo->url);
            if (oldName != currentsubInfo->name)
            {
               if (currentFileName != "")
               {
                  CDataBase db(currentFileName);
                  CSubscriptionInfo subinfo;
                  rc = db.Init();
                  if (rc == E_DB_OK)
                  {
                     rc = db.GetInfoTableData(&subinfo);
                     DBG_INFO("Updating name");
                     subinfo.name = currentsubInfo->name;
                     rc = db.UpdateInfoTableData(subinfo);
                  }
               }
            }
            ((MainFrame*)wxGetApp().GetMainFrame())->RefreshInterface(1,currentsubInfo->url);
            Repopulate(currentsubInfo->url);
            wxMessageBox(_("Subscription modified"), _("Update"), wxOK | wxICON_INFORMATION, NULL);
         }
      }
      editDialog->Destroy();
      delete editDialog;
      Repopulate();
   }
}


void CSubscriptionListDialog::OnUpdateButtonClick( wxCommandEvent& event )
{
   int rc;
   CBusyGuard guard(this);
   if (guard.Allows())
   {
      SetBusy();
      int index = m_SubscriptionDataList->GetSelectedRow();
      if (index != wxNOT_FOUND)
      {
         CSubscriptionInfo *subInfo = (*m_subscriptionList)[index]->GetSubscriptionInfo();
         rc = UpdateSubscription((*m_subscriptionList)[index]);
         if (rc != E_DLP_OK)
         {
            wxMessageBox(_("Could not update the selected subscription"), _("Error"), wxOK | wxICON_ERROR, NULL);
         }
         else
         {
            ((MainFrame*)wxGetApp().GetMainFrame())->RefreshInterface(1, subInfo->url);
            Repopulate(subInfo->url);
            wxMessageBox(_("Subscription Updated"), _("Update"), wxOK | wxICON_INFORMATION, NULL);
         }
      }
   }
  
}

void CSubscriptionListDialog::OnDeleteClicked( wxCommandEvent& event ) 
{
    
   CBusyGuard guard(this);
   if (guard.Allows())
    { 
       int index = m_SubscriptionDataList->GetSelectedRow();
       if (index != wxNOT_FOUND)
       {
          CSubscription *sub = (*m_subscriptionList)[index];
          wxString deleteFileName = CSubscription::FindCachedData(sub->GetSubscriptionInfo()->url);
          if (deleteFileName != "")
          {
             if (wxMessageBox(_("Do you really wish to delete the selected subscription, ") + sub->GetSubscriptionInfo()->name +"?",
                              _("Question"), 
                                wxYES_NO|wxICON_QUESTION, NULL) == wxYES)
             {
               wxRemoveFile(deleteFileName);
              
               ((MainFrame*)wxGetApp().GetMainFrame())->RefreshInterface(1);
                Repopulate();
                
             }
          }
       }
    }
       
}

void CSubscriptionListDialog::Repopulate(wxString pathSel)
{
   unsigned int listCount;
   unsigned int index = wxNOT_FOUND;
   DBG_INFO("pathSel is: %s", (const char*)pathSel.utf8_str());

   listCount=m_subscriptionList->GetCount();
   m_SubscriptionDataList->DeleteAllItems();
   if (listCount != 0)
   {
      for (unsigned int i = 0; i < listCount; i++)
      {
         wxVector<wxVariant>data;
         data.push_back((*m_subscriptionList)[i]->GetSubscriptionInfo()->name);
         data.push_back((*m_subscriptionList)[i]->GetSubscriptionInfo()->url);
         m_SubscriptionDataList->AppendItem(data);
         if ((*m_subscriptionList)[i]->GetSubscriptionInfo()->url == pathSel)
         {
            index = i;
            DBG_INFO("Found subscription at index = %d", index)
         }
            
         data.clear();
      }
      m_SubscriptionDataList->SelectRow(index);
      
   }
}

void CSubscriptionListDialog::OnXtreamButtonClicked(wxCommandEvent& event)
{
   CBusyGuard guard(this);
   CSubscription* newSub = wxGetApp().GetMainFrame()->GetTempSubscription();
   if (!newSub)
   {
      newSub = new CSubscription("");
      wxGetApp().GetMainFrame()->SetTempSubscription(newSub);
   }   
   if (guard.Allows())
   { 
      SetBusy();
      CXtreamDlg *xtreamDialog = new CXtreamDlg(this);
      if (xtreamDialog->ShowModal() == wxID_OK)
      {
         CSubscriptionInfo *newsubInfo = newSub->GetSubscriptionInfo();
         xtreamDialog->GetData(newsubInfo);
         if (CSubscription::FindCachedData(newsubInfo->url) != "")
         {
            wxMessageBox(_("Found another subscription with the same URL"), _("Error"), wxOK | wxICON_ERROR, NULL);
         }
         else
         {
            newSub->SetURI(newsubInfo->url);
            if (UpdateSubscription(newSub) != E_DLP_OK)
            {
               wxMessageBox(_("Could not set up the selected subscription"), _("Error"), wxOK | wxICON_ERROR, NULL);
            }
            else
            {
               ((MainFrame*)wxGetApp().GetMainFrame())->RefreshInterface(1, newsubInfo->url);
               Repopulate(newsubInfo->url);
               wxMessageBox(_("Subscription set up"), _("Update"), wxOK | wxICON_INFORMATION, NULL);
            }
         }
         
      }
      xtreamDialog->Destroy();
      delete xtreamDialog;
   }
}

int CSubscriptionListDialog::UpdateSubscription(CSubscription *subscription)
{
   TSubscriptionList list;
   list.Add(subscription);
   
   CDLProgress *progressDialog = new CDLProgress(this, &list);
   int rc = progressDialog->ShowModal(); 
   
   progressDialog->Destroy();
   delete progressDialog;
   return rc;
}    
