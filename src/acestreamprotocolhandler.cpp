#include <rapidjson/document.h> 
#include <rapidjson/prettywriter.h> // for stringify JSON
#include <wx/file.h>
#include <wx/filename.h>
#include <wx/dir.h>
#include <wx/msgdlg.h>
#include <wx/timer.h>
#include "debug.h"
#include "main.h"
#include "downloadclient.h"
#include "acestreamprotocolhandler.h"
#include "configuration.h"



using namespace tvlite;
using namespace rapidjson;

#define E_ERROR -1
#define E_OK 0


CAcestreamProtocolHandler::CAcestreamProtocolHandler(wxEvtHandler *parent, wxString url, wxArrayString vlcoptions) : 
  CBaseProtocolHandler(parent, url, vlcoptions),
  m_dlClient(NULL),
  m_isLocked(false),
  m_timer(this)
 
{   
   Connect(wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(CAcestreamProtocolHandler::OnTimer));
}


CAcestreamProtocolHandler::~CAcestreamProtocolHandler()
{
   if (m_dlClient)
   {
      m_dlClient->SetEventHandler(NULL);
   }
   DBG_INFO("ACE Protocol Handler -> deleted");
}

void CAcestreamProtocolHandler::Start()
{
   InitURLs();
}

void CAcestreamProtocolHandler::Stop()
{
   Disconnect(DLNOTIFY_EVT, wxThreadEventHandler(CAcestreamProtocolHandler::OnInitURLsResponse));
   SendStop();
   
}

int CAcestreamProtocolHandler::InitURLs()
{
   /*if (m_isLocked)
   {
      DBG_WARNING("ACE Protocol Handler locked. Doing nothing");
      return E_OK;
   }*/
   if (m_dlClient)
   {
       m_dlClient->SetCanceled(true);
       return(E_OK);
   } 
   m_dlClient = new CDownloadClient(this);
   wxString importFileName = wxFileName(CConfiguration::GetTempDir(), "_jsontemp_").GetFullPath();
   int rc = E_OK;
   
   if (!m_dlClient)
   {
      rc = E_ERROR;   
   }
   
   if (rc == E_OK)
   {
      rc = m_dlClient->InitCurl();
   }
   ((MainFrame*)wxGetApp().GetMainFrame())->SetStatusText(_("Requesting data from Acestream"), 0);
   if (rc == E_OK)  
   {
      if (m_dlClient->Download(m_url, importFileName))
      {
          wxMessageBox(_("Error retrieving data from Acestream engine"),_("Error"), wxOK|wxCENTER|wxICON_ERROR);
          ((MainFrame*)wxGetApp().GetMainFrame())->SetStatusText(_("Ready"), 0);
          rc = E_ERROR;
      }   
   }
   if (rc == E_OK)
   {
      Connect(wxID_ANY, DLNOTIFY_EVT, wxThreadEventHandler(CAcestreamProtocolHandler::OnInitURLsResponse));
   }
   //m_isLocked = true;
   return rc;
}

void CAcestreamProtocolHandler::OnInitURLsResponse(wxThreadEvent &evt)
{   
   wxString importFileName = wxFileName(CConfiguration::GetTempDir(), "_jsontemp_").GetFullPath();
   wxFile importFile;
   wxString jsonResponse;
   wxString errResp;
   int rc = E_OK;
   DBG_INFO("Enter URL response  handler");  
   Disconnect(DLNOTIFY_EVT, wxThreadEventHandler(CAcestreamProtocolHandler::OnInitURLsResponse));
   //m_isLocked = false;
   if (evt.GetInt() == 0)
   {
      importFile.Open(importFileName);
      if (!importFile.IsOpened())
      {
         DBG_ERROR("Error opening temporary file");
         errResp = "Error opening temporary file";
         rc = E_ERROR;
      }
   }
   else
   {
        DBG_ERROR("Error Communicating with acestream");
        errResp = "Error Communicating with acestream";
        rc = E_ERROR;
   }
   
   if (rc == E_OK)
   {
      if (!importFile.ReadAll(&jsonResponse))
      {
         DBG_ERROR("Could not read temporary file to string");
         rc = E_ERROR;
      }
   }
   
   if (rc == E_OK)
   {
      rc = E_ERROR;
      DBG_INFO("String is %s", (const char*)jsonResponse.utf8_str());  
      Document doc;
        doc.Parse((const char*)jsonResponse.utf8_str());
        if (doc.IsObject())
        {
           if (doc.HasMember("error") && doc["error"].IsString())
           {
              errResp = wxString(doc["error"].GetString());
              if (errResp == "failed to load content" && m_url.Matches("*getstream*"))
              {
                 DBG_WARNING("Retrying with HLS");
                 m_url.Replace("getstream", "manifest.m3u8");
                 m_url.Replace("?id","?infohash");
                 m_dlClient = NULL;
                 InitURLs();
                 return;
              }
           }
           else if (doc.HasMember("response") && doc["response"].IsObject())
           {
                 rc = E_OK;
                 if (doc["response"].HasMember("playback_url") && doc["response"]["playback_url"].IsString())
                  {
                    m_streamurl = wxString(doc["response"]["playback_url"].GetString());
                    DBG_INFO("Playback URL is %s", (const char*)m_streamurl.utf8_str());
                    m_vlcPlayer->Play(m_streamurl, m_vlcoptions);
#ifdef __WXMSW__
                    //((MainFrame*)wxGetApp().GetTopWindow())->RaiseTransp();
#endif
                  
                  }
                  if (doc["response"].HasMember("command_url") && doc["response"]["command_url"].IsString())
                  {
                    m_commandurl = wxString(doc["response"]["command_url"].GetString());
                    DBG_INFO("Command URL is %s", (const char*)m_commandurl.utf8_str());
                  }
                  
           }
 
         } 
   }
   
  
   m_dlClient = NULL;
   if (rc == E_ERROR) //acestream sent bullshit
   {
      wxMessageBox(_("Error response from Acestream engine\n") + errResp,_("Error"), wxOK|wxCENTER|wxICON_ERROR);
      //((MainFrame*)wxGetApp().GetMainFrame())->SetStatusText(_("Acestream sent an empty response"), 0);
      OnStopAsync();
   }
}

int CAcestreamProtocolHandler::SendStop()
{
    if (m_dlClient)
    {
       //Forces stopping a second time
       DBG_INFO("Stopped during Acestream communication. Forcing stop.");
       m_dlClient->SetCanceled(true);
       StopPlayer();
       Disconnect(DLNOTIFY_EVT, wxThreadEventHandler(CAcestreamProtocolHandler::OnInitURLsResponse));
       Disconnect(DLNOTIFY_EVT, wxThreadEventHandler(CAcestreamProtocolHandler::OnStopResponse));
       m_dlClient = NULL;
       return E_OK;
    } 
    
    m_dlClient = new CDownloadClient(this);
    wxString sendurl;
    wxString importFileName = wxFileName(CConfiguration::GetTempDir(), "_jsontemp_").GetFullPath();
    int rc = E_OK;
    if  (m_commandurl == "")
    {
       rc = E_ERROR;
    }
    if (rc == E_OK)
    {
       sendurl = m_commandurl + "?method=stop";
       if (m_dlClient->InitCurl())
      {
         rc =  E_ERROR;
      }
    }
   if (rc == E_OK)  
   {
      DBG_INFO("Sending stop to %s", (const char*)sendurl.utf8_str());  
      if (m_dlClient->Download(sendurl, importFileName))
      {
          wxMessageBox(_("Error retrieving data from Acestream engine"),_("Error"), wxOK|wxCENTER|wxICON_ERROR);
          rc =  E_ERROR;
      }   
   }

   if (rc == E_OK)
   {
      Connect(wxID_ANY, DLNOTIFY_EVT, wxThreadEventHandler(CAcestreamProtocolHandler::OnStopResponse));
      m_timer.Start(2000, wxTIMER_ONE_SHOT);
   }
   else
   {
      StopPlayer();
   }
   return rc;
}


void CAcestreamProtocolHandler::OnStopResponse(wxThreadEvent &evt)
{
   DBG_INFO("Enter stop response  handler");  
   Disconnect(DLNOTIFY_EVT, wxThreadEventHandler(CAcestreamProtocolHandler::OnStopResponse));
   m_dlClient = NULL;
   //m_isLocked = false;
   if (m_timer.IsRunning())
   {
      m_timer.Stop();
   }
   StopPlayer();
}

void CAcestreamProtocolHandler::OnTimer(wxTimerEvent &evt)
{
   
   DBG_INFO("Enter stop timeout  handler");  
   if (m_dlClient != NULL)
   {
      m_dlClient->SetEventHandler(NULL);
   }
   m_dlClient = NULL;
   //m_isLocked = false;
   StopPlayer();
}

void CAcestreamProtocolHandler::StopPlayer()
{
   m_vlcPlayer->Stop();
   m_stopped = true;
   OnStopAsync();
}