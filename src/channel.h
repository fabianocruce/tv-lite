#ifndef CCHANNEL_H
#define CCHANNEL_H
#include <string>
#include <vector>
#include <wx/string.h>
#include <wx/dynarray.h>
#include <wx/hashmap.h>
#include <wx/arrstr.h>

//struct SParamRecord
//{
//  wxString sUrl;
//  wxString sName;
//  
//};

namespace tvlite
{

WX_DECLARE_STRING_HASH_MAP(wxArrayString, CStringToArrayStringHashMap);

class CChannel
{
public:
   CChannel();
   CChannel(wxString id, wxString name, wxArrayString streamurls, wxString iconfile);
   ~CChannel();
   wxString GetName();
   void SetName(wxString name);
   wxString GetId();
   void SetId(wxString id);
   wxArrayString GetStreamURLs();
   wxArrayString GetAudioChannels();
   void SetStreamURLs(wxArrayString);
  
   wxStringToStringHashMap GetStreamNames();
   CStringToArrayStringHashMap GetVLCOptions();
   wxArrayString GetVLCOptions(wxString sUrl);
   void SetVLCOptions(wxString sURL, wxArrayString& options);
   
   void SetGroup(wxString group);
   wxString GetGroup();

   void SetStreamNames(wxStringToStringHashMap s);
   void SetVLCOptions(CStringToArrayStringHashMap s);

   void SetAudioChannels(wxArrayString);
   int ConvertFromJSONArray(wxString s, wxArrayString& sURLs);
   int ConvertFromJSONParam(wxString s);
   
   void ConvertToJSONArray(wxArrayString sURLs, wxString& s);
   wxString ConvertToJSONParam();
   
   static int CompareAsc( CChannel **item1, CChannel **item2);
   static int CompareDesc( CChannel **item1, CChannel **item2);
   void PopulateDefaultValues(wxArrayString& sURLs, wxStringToStringHashMap& sParams);
   void PopulateDefaultValues();
   wxString AssignDefaultValue(wxString uri);

private:
   wxString m_id;
   wxString m_name;
   wxString m_group;
   wxArrayString m_streamurls;
   
   CStringToArrayStringHashMap m_vlcoptrecords;
   wxStringToStringHashMap m_namerecords;
   
   wxArrayString m_audiochannels;
   wxString m_icon;
};

WX_DECLARE_OBJARRAY(CChannel, TChannelList);

}

#endif // CCHANNEL_H
