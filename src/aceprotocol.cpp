#include "aceprotocol.h"
#include "configuration.h"
#include "main.h"
#include "debug.h"
#ifdef __WXMSW__
#include <wx/stdpaths.h>
#include <wx/filename.h>
#endif
#define E_ERROR -1
#define E_OK 0


using namespace tvlite;

CAceProtocol::CAceProtocol(wxEvtHandler *parent, wxString url, wxString cmd):
   CBaseProtocol(parent,url, cmd), m_IsInstalled(false)
   {
      //Redirect();
   }


tvlite::CAceProtocol::~CAceProtocol()
{ 
   DBG_INFO ("ACE protocol -> delete"); 
}


int  CAceProtocol::StartProtocol (long &pid)
{ 
   CConfiguration *conf = CConfiguration::Instance();
   DBG_INFO("Starting acestream engine process");
   MainFrame *mainWindow = (MainFrame*)wxGetApp().GetMainFrame();
   pid = 0;
   int result = E_OK;
   m_cmd = conf->GetAceConfiguration().GetFullPath();
   if (m_cmd == "")
   {
      if (SearchAce(m_cmd) != 0)
      {
         //wxMessageBox(_("Could not search for the Acestream binary.\n Check your installation!"), _("Error"), wxOK|wxICON_EXCLAMATION, mainWindow);
         result = E_ERROR;
      }
      else
      {
         if (m_cmd == "")
         {
            wxMessageBox(_("Could not find the Acestream engine binary in the path."), _("Error"), wxOK|wxICON_EXCLAMATION, mainWindow);
            result = E_ERROR;
         }
         else
         {
            conf->GetAceConfiguration().SetFullPath(m_cmd);
         }   
      }
         
   }
   
   if (result == E_OK)
   {
      m_IsInstalled = true;
      unsigned long cacheSize = conf->GetAceConfiguration().GetCacheSize();
      wxString sCacheType;
      int uiCacheType = conf->GetAceConfiguration().GetCacheType();
      if (uiCacheType == E_CACHE_DISK)
      {
         sCacheType = "disk";
      }
      else
      {
         sCacheType = "memory";
      }
         
#ifdef __WXMSW__
      wxString cmd = m_cmd << " --proxy-server-main --live-cache-type=" << sCacheType << " --vod-cache-type=" << sCacheType << 
    " --disk-cache-limit=" << cacheSize << " --memory-cache-limit=536870912";
#else    

      wxString cmd = m_cmd << " --client-console --live-cache-type=" << sCacheType << " --vod-cache-type=" << sCacheType << 
    " --disk-cache-limit=" << cacheSize << " --memory-cache-limit=536870912";
#endif      
      DBG_INFO("Acestream command is '%s'.", (const char*)cmd.utf8_str());
      long rc = wxExecute(cmd, wxEXEC_ASYNC|wxEXEC_MAKE_GROUP_LEADER, this);
      pid = rc;
      if (rc == 0L)
      {
         DBG_ERROR("Execution of '%s' failed.", (const char*)cmd.utf8_str());
         wxMessageBox(_("Could not execute Acestream engine!"), _("Error"), wxOK|wxICON_EXCLAMATION, mainWindow);
         result = E_ERROR;
      }
   }
   else
   {
      m_IsInstalled = false;
   }
  
   return result;
}


int CAceProtocol::SearchAce(wxString &cmd)
{
   wxArrayString res, err;
#ifdef __WXMSW__
     int rc;
     wxStandardPaths stdPath = wxStandardPaths::Get();
     wxString AceDir = stdPath.GetUserConfigDir() + "\\ACEStream\\engine";
     wxFileName cmdFileName(AceDir, "ace_engine.exe");
     DBG_INFO("Engine supposed name %s", (const char*)AceDir.utf8_str());
     if (cmdFileName.IsOk()  && cmdFileName.FileExists())
     {
         rc = 0;
         cmd = cmdFileName.GetFullPath();
     }   
     else
     {
         rc = -1;
     }
#else
   long rc = wxExecute("which acestreamplayer.engine" , res, err);
   if (!rc)
   {
      if (!res.IsEmpty())
      {
         cmd = res[0];
      }   
   }
#endif   
   return rc;
}

int CAceProtocol::LoadConfig()
{
   //TODO stub
   return 0;   
}


int CAceProtocol::SaveConfig()
{
   //TODO stub
   return 0;
}

void CAceProtocol::StopProtocol()
{
     DBG_INFO("Enter stop protocol");
     if (Exists(this->GetPid()))
     {
        DBG_INFO("Killing current acestream process"); 
        wxProcess::Kill((int)this->GetPid(), wxSIGTERM, wxKILL_CHILDREN );
     }
     else
     {
        DBG_WARNING("No acestream process? Maybe terminated...")
     }
     wxProcessEvent event(wxID_ANY, this->GetPid());
     wxQueueEvent(m_pparent, event.Clone());
     DBG_INFO("Exit stop protocol");
}

/*void CAceProtocol::OnTerminate(int pid, int status)
{
   
   DBG_INFO("On Terminate entered");  
   DBG_INFO("Process %u ('%s') terminated with exit code %d.",
                pid, (const char*)m_cmd.c_str(), status);
   wxProcess::OnTerminate(pid,status);             
}*/