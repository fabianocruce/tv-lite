#include <ostream>
#include "channelinfo.h"
#include "channel.h"

using namespace tvlite;
using namespace std;

CChannelInfo::CChannelInfo(wxWindow* parent) : CChannelInfoBase(parent)
{

}

CChannelInfo::~CChannelInfo()
{

}


void CChannelInfo::SetData(CChannel *channel)
{
   wxTextAttr attr = m_InfoTextCtrl->GetDefaultStyle();
   attr.SetFontFamily(wxFONTFAMILY_TELETYPE);
   m_InfoTextCtrl->SetDefaultStyle(attr);
   m_InfoTextCtrl->AppendText(_("Channel name: ") + channel->GetName() + "\n") ;
   m_InfoTextCtrl->AppendText(_("Channel group: ") + channel->GetGroup() + "\n");
   for (size_t i = 0; i < channel->GetStreamURLs().GetCount(); i++ )
   {
      
     m_InfoTextCtrl->AppendText(channel->GetStreamNames()[channel->GetStreamURLs()[i]] + " : " + channel->GetStreamURLs()[i] + "\n"); 
   }
}

void CChannelInfo::OnOkClicked( wxCommandEvent& event )
{
       EndModal(wxID_OK);
}