#ifndef _USER_DATA_OBJECT_H
#define _USER_DATA_OBJECT_H

#include <wx/object.h>

class UserDataObject:public wxObject
{
public:
     UserDataObject():m_data(NULL) {};
     UserDataObject(void *data):m_data(data){};
     ~UserDataObject()
     {
        m_data = NULL;
     }
     ;
     void * GetData() 
     {
       return m_data;
     };
private:
      void *m_data;      
};

#endif /*_USER_DATA_OBJECT_H */