#ifndef CSUBSCRIPTIONLISTDIALOG_H
#define CSUBSCRIPTIONLISTDIALOG_H

#include <wx/wx.h>
#include <wx/window.h> 
#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif
#include "main.h"
#include "subscription.h"

namespace tvlite
{ 

class CSubscriptionListDialog : public CSubscriptionListDialogBase, public CBusyObject
{
private:
   CSubscriptionListDialog(const CSubscriptionListDialog& rhs) = delete;
   CSubscriptionListDialog& operator=(const CSubscriptionListDialog& rhs) = delete;
   TSubscriptionList *m_subscriptionList;
   int UpdateSubscription(CSubscription *subscription);
   CSubscription * m_tempsubscription; //for updating 
            

public:
   CSubscriptionListDialog(wxWindow *parent);
   virtual ~CSubscriptionListDialog();
   void Repopulate(wxString pathSel = "");
   virtual void OnNewClicked( wxCommandEvent& event ); 
   virtual void OnEditClicked( wxCommandEvent& event ); 
	virtual void OnDeleteClicked( wxCommandEvent& event ); 
   virtual void OnUpdateButtonClick( wxCommandEvent& event );
   virtual void OnXtreamButtonClicked( wxCommandEvent& event );

};

}
#endif // CSUBSCRIPTIONLISTDIALOG_H
