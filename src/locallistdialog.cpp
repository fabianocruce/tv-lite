#include "locallistdialog.h"
#include "localdialog.h"
#include "locallist.h"

using namespace tvlite;

CLocalListDialog::CLocalListDialog(wxWindow* parent):CLocalListDialogBase(parent)
{
   m_LocalDataList->InsertColumn(0, "Nume Lista");
   m_closeButton->SetDefault();
}

CLocalListDialog::~CLocalListDialog()
{

}

void CLocalListDialog::SetLists(TLocalLists * localLists)
{
   m_localLists = localLists;
}

void CLocalListDialog::OnNewClick( wxCommandEvent& event )
{
   CBusyGuard guard(this);
   if (guard.Allows())
   {
      CLocalDialog *dialog = new CLocalDialog(this);
      int rc = dialog->ShowModal();
      if (rc == wxID_OK)
      {
         CLocalList newList;
         CSubscriptionInfo info;
         dialog->GetData(&info);
         newList.SetSubscriptionInfo(info);
         wxString path;
         newList.SaveDataToListDir(path); //TODO check result
         ((MainFrame*)wxGetApp().GetTopWindow())->RefreshInterface(0);
         Repopulate(path);
      }
      dialog->Destroy();
   }
}

void CLocalListDialog::OnEditClick( wxCommandEvent& event )
{
   CBusyGuard guard(this);
   if (guard.Allows())

   {
      long itemIndex = -1; 
      itemIndex= m_LocalDataList->GetNextItem(itemIndex,
             wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
      if (itemIndex != wxNOT_FOUND)
      { 
         CLocalDialog *dialog = new CLocalDialog(this);
         CLocalList *item = m_localLists->Item(itemIndex);
         CSubscriptionInfo *info = item->GetSubscriptionInfo();
         wxString path = item->GetURI();
         dialog->SetData(info);
         int rc = dialog->ShowModal();
         if (rc == wxID_OK)
         {
            dialog->GetData(info);
            item->SetSubscriptionInfo(*info);
            item->UpdateInfo(*info);
            ((MainFrame*)wxGetApp().GetTopWindow())->RefreshInterface(0);
            Repopulate(path);
            
         }
         dialog->Destroy();
      }
   }
   
}

void CLocalListDialog::OnDeleteClick( wxCommandEvent& event )
{
   CBusyGuard guard(this);
   if (guard.Allows())

   {
      SetBusy();
      long itemIndex = -1; 
      itemIndex= m_LocalDataList->GetNextItem(itemIndex,
             wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
      if (itemIndex != wxNOT_FOUND)
      {
         CLocalList *item = m_localLists->Item(itemIndex);
         
         if (wxMessageBox(_("Do you really wish to delete the selected list, ") + item->GetSubscriptionInfo()->name +"?",
                                 _("Question"), 
                                   wxYES_NO|wxICON_QUESTION, NULL) == wxYES)
         {
             wxRemoveFile(item->GetURI());
             ((MainFrame*)wxGetApp().GetTopWindow())->RefreshInterface(0);
             Repopulate();
         }
      }
   }
}     

void CLocalListDialog::Repopulate(wxString pathSel)
{
   long itemIndex = 0;
   m_LocalDataList->DeleteAllItems();
   if (m_localLists->GetCount() != 0)
   { 
      for (unsigned int i = 0; i < m_localLists->GetCount(); i++)
      {
         wxListItem item;
         item.SetId((long)i);
         item.SetText((*m_localLists)[i]->GetSubscriptionInfo()->name);
         m_LocalDataList->InsertItem(item);
         if (pathSel == (*m_localLists)[i]->GetURI())
         {
            itemIndex = i;
         }
      }
       m_LocalDataList->SetColumnWidth(0,wxLIST_AUTOSIZE); 
       m_LocalDataList->SetItemState(itemIndex,wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
   }
}
