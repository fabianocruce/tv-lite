#ifndef CPREFDIALOG_H
#define CPREFDIALOG_H

#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif

namespace tvlite
{
   
#define RBNONE 0
#define RBANY  1
#define RBCUSTOM 2

class CPrefDialog : public CPrefDialogBase
{
   public:
   CPrefDialog() = delete;
   CPrefDialog(wxWindow *parent);
   virtual ~CPrefDialog();
   //fills in the dialog box with the values from configuration
   void SetData(); 
   //Gets the data from the dialog box and writes it into config
   void GetData();
   bool ValidateData(); 
   virtual void OnRecordingPathClick( wxCommandEvent& event ); 
   virtual void OnHwAccType( wxCommandEvent& event ); 
	virtual void OnAcestreamExecPathClick( wxCommandEvent& event );
	virtual void OnOkClicked( wxCommandEvent& event );
   virtual void OnProxyUseClick(wxCommandEvent& event);
   virtual void OnProxyAuthCheck(wxCommandEvent& event);
   private:
   void CheckProxyUsage(bool state);
   void CheckAuthUsage (bool state);
};

}

#endif // CPREFDIALOG_H
