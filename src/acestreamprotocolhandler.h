#ifndef CACESTREAMPROTOCOLHANDLER_H
#define CACESTREAMPROTOCOLHANDLER_H

#include "baseprotocolhandler.h"
#include "downloadclient.h"

namespace tvlite
{

class CAcestreamProtocolHandler : public CBaseProtocolHandler
{
private:
   CAcestreamProtocolHandler(const CAcestreamProtocolHandler& rhs);
   CAcestreamProtocolHandler& operator=(const CAcestreamProtocolHandler& rhs);
   CAcestreamProtocolHandler();
   int InitURLs();
   int SendStop();
   wxString m_streamurl;
   wxString m_commandurl;
   CDownloadClient *m_dlClient;
   bool m_isLocked;
   wxTimer m_timer;
   void StopPlayer();
   
public:
   CAcestreamProtocolHandler(wxEvtHandler *parent, wxString url, wxArrayString vlcoptions);
   virtual ~CAcestreamProtocolHandler();
   virtual void Start() ;
   virtual void Stop() ;
   wxString GetStreamURL();
   wxString GetCommandURL();
   void OnInitURLsResponse(wxThreadEvent &evt);
   void OnStopResponse(wxThreadEvent &evt);
   void OnTimer(wxTimerEvent &evt);
   

};

}

#endif // CACESTREAMPROTOCOLHANDLER_H
