#include "wx/filename.h"
#include "wx/filefn.h"
#include "wx/msgdlg.h"
#include <string.h>
#include "database.h"
#include "debug.h"
#include "subscriptioninfo.h"
#include "channel.h"
#include "main.h"
#include "statusgauge.h"


using namespace tvlite;

int CDataBase::m_number_of_channels = 0;

CDataBase::~CDataBase()
{
   if (m_db)
   {
      int result = sqlite3_close(m_db);
      DBG_INFO("Database close result %d ", result);
   }
}

int CDataBase::Init()
{
   int result = E_DB_OK;
   wxFileName ffile(m_name);

   if (!wxFileExists(m_name))   
   {
       //DBG_INFO("Database file does not exist. Creating");
       wxString dirname = ffile.GetPath();
       if (!wxDirExists(dirname))
       {
          //DBG_INFO("Directory does not exist. Creating");
          if (!wxMkdir(dirname))
          {
             wxMessageBox (wxT("Could not create directory"),
             wxT("Error!"), wxOK | wxICON_STOP, NULL);
             result = E_DB_CREATE; 		   
          }
       }
   }
   if (result == E_DB_OK)
   {
       result = sqlite3_open((const char*)m_name.utf8_str(), &m_db);
       if (result != E_DB_OK)
       {
          DBG_ERROR("Database error %s", sqlite3_errmsg(m_db));
       }
       else
       {
          //DBG_INFO("Initialising database tables");
       }
   }
   
   if (result == E_DB_OK)
   {
      result = InitInfoTable();
   }
   
   if (result == E_DB_OK)
   {
    result = InitTVTable();
   }
   if (result == E_DB_OK)
   {
    result = InitRadioTable();
   }
 
   return result;
}

int CDataBase::InitTable(wxString name, wxString stmt /*= ""*/, bool create /*= false*/)
{
   char *errmsg;
   wxString checkstmt = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + name + "';";
   TCallbackData cbData(this, name);
   int rc = sqlite3_exec(m_db, (const char*)checkstmt.utf8_str() ,tab_callback, (void*)&cbData, &errmsg);
   if (rc)
   {
      wxMessageBox("While checking " + name + " table:\n " + wxString(errmsg,wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
      return rc;
   }  
   if (!cbData.tablefound)
   {
      if (create)
      {
         //DBG_INFO("Table %s not found. Creating", (const char*)name.utf8_str());
         wxString createstmt = "CREATE TABLE " + name + " (" + stmt +");";
         rc = sqlite3_exec(m_db,createstmt.utf8_str(), NULL, NULL, &errmsg);
         if (rc) 
         {
            wxMessageBox("While creating " + name + " table:\n " + wxString (errmsg, wxConvUTF8) , "Error", wxOK | wxICON_STOP, NULL);
            return rc;
         }
      }
   }

 return rc;

}

int CDataBase::DeleteTable(wxString name)
{
   char *errmsg;
   wxString checkstmt = "DROP TABLE IF EXISTS " + name + ";";
   int rc = sqlite3_exec(m_db, (const char*)checkstmt.utf8_str(), NULL, NULL, &errmsg);
   if (rc)
   {
      wxMessageBox("While deleting " + name + " table:\n " + wxString(errmsg,wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
   }  
   return rc;
}


int CDataBase::InitInfoTable()
{
   int result;
   //DBG_INFO("Initialising database information");
   result = InitTable("info", "name, version, author, url, epgurl", true);
   return result;
}

int CDataBase::InitTVTable()
{
   //DBG_INFO("Initialising TV channel info");
   return InitTable("tv_channels", "id, icon, name, streamurls, params, guide, audiochannels", true);
}

int CDataBase::InitRadioTable()
{
    //DBG_INFO("Initialising Radio channel info");

    return InitTable("radio_channels", "id, icon, name, streamurls, params", true);
}

int CDataBase::tab_callback(void *data, int nr_cols, char **arg, char **colname)
{
   TCallbackData *cbdata = (TCallbackData*)data;
   if (cbdata == nullptr) 
   {
      return -1;
   }
   if (!strncmp(*arg, (const char*)cbdata->tablename.utf8_str(), strlen(*arg)))
   {
      cbdata->tablefound = true;
	}
   return 0;
}

int CDataBase::GetInfoTableData(CSubscriptionInfo *dataBaseInfo)
{
   //DBG_INFO("Entering info table.");
   char *errmsg;
   wxString checkstmt = "SELECT * FROM info;";
   int rc = sqlite3_exec(m_db, (const char*)checkstmt.utf8_str() ,info_callback, (void*)dataBaseInfo, &errmsg);   
   if (rc)
   {
      wxMessageBox("While retrieving channel database information:\n " + wxString(errmsg,wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
   }
   return rc;
}


int CDataBase::GetNumberOfChannels()
{
   char *errmsg;
   wxString checkstmt = "SELECT count(*) FROM tv_channels;";
   int rc = sqlite3_exec(m_db, (const char*)checkstmt.utf8_str(), number_callback, (void*)&m_number_of_channels, &errmsg); 
   if (rc)
   {
      wxMessageBox("While retrieving channel number:\n " + wxString(errmsg,wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
   }
   return rc;  
   
}


int CDataBase::number_callback(void *data, int nr_cols, char **arg, char **colname)
{
   if (data == nullptr)
   {
      DBG_ERROR("No info argument given.");
      return E_DB_QUERY;
   }
   if (nr_cols != 1)
   {
      DBG_ERROR("Wrong number of columns");
      return E_DB_QUERY;
   }  
   int *number = (int*)data;
   sscanf (arg[0], "%d", number);
   return E_DB_OK;
    
}

int CDataBase::info_callback(void *data, int nr_cols, char **arg, char **colname)
{
   //DBG_INFO("Entering info callback.");
   CSubscriptionInfo* dbInfo = (CSubscriptionInfo*) data;
   if (dbInfo == nullptr) 
   {
      DBG_ERROR("No info argument given.");
      return E_DB_QUERY;
   }
   if (nr_cols != 5)
   {
      DBG_ERROR("Wrong number of columns");
      return E_DB_QUERY;
   }
   dbInfo->name = wxString::FromUTF8((const char*)arg[0]);
   dbInfo->version = wxString::FromUTF8((const char*)arg[1]);
   dbInfo->author = wxString::FromUTF8((const char*)arg[2]);
   dbInfo->url = wxString::FromUTF8((const char*)arg[3]);
   DBG_INFO("[DEBUG] dbInfo.url is %s ", (const char*)wxString::FromUTF8((const char*)arg[3]).utf8_str()); 
   DBG_INFO("[DEBUG] dbInfo.url is %s ", (const char*)dbInfo->url.utf8_str());
   dbInfo->epgurl = wxString::FromUTF8((const char*)arg[4]);
   
   return E_DB_OK;
}

int CDataBase::GetTVTableData(TChannelList *channelList)
{
   //DBG_INFO("Entering info table.");
   char *errmsg;
   int rc = GetNumberOfChannels();
   if (!rc)
   {
      wxString checkstmt = "SELECT * FROM tv_channels;";
      int rc = sqlite3_exec(m_db, (const char*)checkstmt.utf8_str() ,channel_callback, (void*)channelList, &errmsg);
      if (rc)
      {
         wxMessageBox("While retrieving channels:\n " + wxString(errmsg,wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
      }
   }
   return rc;
}


int CDataBase::channel_callback(void *data, int nr_cols, char **arg, char **colname)
{
   //DBG_INFO("Entering info callback.");
   CChannel channel;
   TChannelList* channelList = (TChannelList*) data;
   if (channelList == nullptr) 
   { 
      DBG_ERROR("No info argument given.");
      return E_DB_QUERY;
   }

   for (int i = 0; i < nr_cols; i++)
   {
      wxString sColname = colname[i];
      if (sColname == "id")
      {
         channel.SetId(wxString::FromUTF8((const char*)arg[i]));
      }
      else if (sColname == "icon")
      {
         //DBG_INFO("TODO: convert icon");
      }
      else if (sColname == "streamurls")
      {
         wxArrayString s;
         channel.ConvertFromJSONArray(wxString::FromUTF8((const char*)arg[i]), s);
         channel.SetStreamURLs(s);
      }  
      else if (sColname == "name")
      {
         channel.SetName(wxString::FromUTF8((const char*)arg[i]));
      }
      else if (sColname == "params")
      {
         //DBG_INFO("convert params");
         channel.ConvertFromJSONParam(wxString::FromUTF8((const char*)arg[i]));
      }
//      else if (sColname == "guide")
//      {
//        channel.SetGuide((const char*)arg[i]);
//      }
//      else if (sColname == "audiochannels")
//      {
//        wxArrayString s;
//        channel.ConvertFromJSONArray(wxString::FromUTF8((const char*)arg[i]),s);
//        channel.SetAudioChannels(s);
//      }
      else
      {
        //DBG_ERROR("Ignoring channel field %s", (const char*)sColname.utf8_str());
      }
   }
   if (channel.GetId() != "") //initialized
   {
      channel.PopulateDefaultValues();
      channelList->Add(channel);
      int index = channelList->GetCount();
      float percent = (float)index/(float)m_number_of_channels * 100;
      if (index % 100 == 0)
      {
         CStatusGauge::UpdateGauge(E_SB_LISTPROGRESS, round(percent));
         wxYield();
      }
   }
   return E_DB_OK;
}


int CDataBase::SetTVTableData(TChannelList &channelList)
{
  
  int rc;
  char *errmsg;
  
  rc = sqlite3_exec(m_db, "BEGIN TRANSACTION", NULL, NULL, &errmsg); 
  if (rc)
  {
     wxMessageBox("While BEGINNING transaction:\n " + wxString(errmsg,wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
  }  
  for (size_t i = 0; i < channelList.GetCount(); i++)
  {
     rc = AddChannel(channelList[i]);
     if (rc) break;
  }
  if (!rc)
  {
      rc = sqlite3_exec(m_db, "COMMIT;", NULL, NULL, &errmsg); 
  }
  else
  {
      rc = sqlite3_exec(m_db, "ROLLBACK;", NULL, NULL, &errmsg); 
  }   
  return rc;
}
   

int CDataBase::AddChannel(CChannel &ch)   
{
   //DBG_INFO("Storing channel.");
   wxString iconData = "iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAJklEQVR4nO3NMQEAAA\
jDMMC/52ECvlRA00nqs3m9AwAAAAAAAJy1C7oDLV5LB/0AAAAASUVORK5CYII=";
   wxString stmt;
   sqlite3_stmt *compiled_stmt; //placeholder
   
   if (ch.GetId() == "")
   {
         wxString id = ch.GetName().BeforeFirst(' ');
         wxString randVal;
         randVal.Printf("%03d", rand() % 1000);
         ch.SetId(id + randVal);
   }
   wxArrayString urls = ch.GetStreamURLs();
   //streamrecordtype params = ch.GetStreamParams();
   wxArrayString achs = ch.GetAudioChannels();

   stmt = "INSERT INTO 'tv_channels' VALUES (@id, @iconData, @name, @streamurls, @params, @guide, @ach);";
   int rc = sqlite3_prepare_v2(m_db, (const char*)stmt.utf8_str(), -1, &compiled_stmt, 0);
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@id");
      wxString stemp = ch.GetId();
      const char* str = (const char*)stemp.utf8_str();
      //DBG_INFO("id = %s", str);
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
      
   } 
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@iconData");
      const char* str = (const char*)(iconData.utf8_str());
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
      
   } 
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@name");
      wxString stemp = ch.GetName();
      const char* str = (const char*)stemp.utf8_str();
      //DBG_INFO("name = %s", str);
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
      
   } 
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@streamurls");
      wxString stemp;
      ch.ConvertToJSONArray(urls, stemp);
      const char* str = (const char*)stemp.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   }  
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@params");
      wxString stemp = ch.ConvertToJSONParam();
      const char* str = (const char*)stemp.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   }  
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@guide");
      wxString stemp = ch.GetId();
      const char* str = (const char*)stemp.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   }  
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@ach");
      wxString stemp; 
      ch.ConvertToJSONArray(achs, stemp);
      const char* str = (const char*)stemp.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   }  
   if (rc == SQLITE_OK)
   {
      rc = sqlite3_step(compiled_stmt);
   }
    
    if (rc != SQLITE_DONE) {
         wxMessageBox("While inserting channels:\n " + wxString(sqlite3_errmsg(m_db),wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
    }
        
    sqlite3_finalize(compiled_stmt);    
     
   return (rc == SQLITE_DONE) ? SQLITE_OK : rc;
}

int CDataBase::UpdateChannel(CChannel &ch)
{
    DBG_INFO("Updating channel.");
    wxString stmt;
    sqlite3_stmt *compiled_stmt; //placeholder
    wxArrayString urls = ch.GetStreamURLs();
    wxArrayString achs = ch.GetAudioChannels();
    
    stmt = "UPDATE tv_channels SET name = @name WHERE id = @id; " ;

    int rc = sqlite3_prepare_v2(m_db, (const char*)stmt.utf8_str(), -1, &compiled_stmt, 0);
    if (rc == SQLITE_OK)
    {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@id");
      wxString stemp = ch.GetId();
      const char* str = (const char*)stemp.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
    } 
   
    if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@name");
      wxString stemp = ch.GetName();
      const char* str = (const char*)stemp.utf8_str();
      DBG_INFO("name = %s", str);
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
      
   } 
   if (rc == SQLITE_OK)
   {
      rc = sqlite3_step(compiled_stmt);
   }
    
   if (rc != SQLITE_DONE) 
   {
      wxMessageBox("While inserting channels:\n " + wxString(sqlite3_errmsg(m_db),wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
   }
        
   sqlite3_finalize(compiled_stmt);
        
   if (rc == SQLITE_DONE)
   {
      stmt = "UPDATE tv_channels SET streamurls = @streamurls, params = @params WHERE id = @id;"; 
      rc = sqlite3_prepare_v2(m_db, (const char*)stmt.utf8_str(), -1, &compiled_stmt, 0);
   }
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@id");
      wxString stemp = ch.GetId();
      const char* str = (const char*)stemp.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   }  
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@streamurls");
      wxString stemp ;
      ch.ConvertToJSONArray(urls, stemp);
      const char* str = (const char*)stemp.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   }  
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@params");
      wxString stemp = ch.ConvertToJSONParam();
      const char* str = (const char*)stemp.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   }  
   if (rc == SQLITE_OK)
   {
      rc = sqlite3_step(compiled_stmt);
   }
    
   if (rc != SQLITE_DONE) 
   {
      wxMessageBox("While updating channels:\n " + wxString(sqlite3_errmsg(m_db),wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
   }
        
   sqlite3_finalize(compiled_stmt); 
   return (rc == SQLITE_DONE) ? SQLITE_OK : rc;

}

int CDataBase::DeleteChannel(CChannel &ch)
{
   //DBG_INFO("Updating channel.");
   char *errmsg;
   wxString stmt;
   stmt = "DELETE FROM tv_channels WHERE id = '" + ch.GetId() + "' ;";
   int rc = sqlite3_exec(m_db, (const char*)stmt.utf8_str(), NULL, NULL, &errmsg);   
   if (rc)
   {
      wxMessageBox("While deleting channels:\n " + wxString(errmsg,wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
   }
   return rc;       
}

int CDataBase::ResetTVTableData()
{
   int rc = DeleteTable("tv_channels");
   if (!rc)
   {
      rc = InitTVTable();   
   }
   return rc;
}


int CDataBase::SetInfoTableData(CSubscriptionInfo &dataBaseInfo)
{
   //DBG_INFO("Storing info.");
   ResetInfoTableData();
 
   wxString stmt;
   sqlite3_stmt *compiled_stmt; //placeholder
  
   stmt = "INSERT INTO 'info' VALUES (@name, @version, @author, @url, @epgurl);"; 
   int rc = sqlite3_prepare_v2(m_db, (const char*)stmt.utf8_str(), -1, &compiled_stmt, 0);
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@name");
      const char* str = (const char*)dataBaseInfo.name.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
      if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@version");
      const char* str = (const char*)dataBaseInfo.version.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
      if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@author");
      const char* str = (const char*)dataBaseInfo.author.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
      if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@url");
      const char* str = (const char*)dataBaseInfo.url.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
      if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@epgurl");
      const char* str = (const char*)dataBaseInfo.epgurl.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
   if (rc == SQLITE_OK)
   {
      rc = sqlite3_step(compiled_stmt);
   }
    
   if (rc != SQLITE_DONE) 
   {
      wxMessageBox("While inserting database info data:\n " + wxString(sqlite3_errmsg(m_db),wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
   }
        
   sqlite3_finalize(compiled_stmt); 
   return (rc == SQLITE_DONE) ? SQLITE_OK : rc;
          
 
}

int CDataBase::ResetInfoTableData()
{
   int rc = DeleteTable("info");
   if (!rc)
   {
      rc = InitInfoTable();   
   }
   return rc;
}

int CDataBase::UpdateInfoTableData(CSubscriptionInfo &dataBaseInfo)
{  
 
  wxString stmt = "UPDATE info SET name = @name, version = @version, author = @author, url = @url, epgurl = @epgurl;";  
  sqlite3_stmt *compiled_stmt; //placeholder 
  
  int rc = sqlite3_prepare_v2(m_db, (const char*)stmt.utf8_str(), -1, &compiled_stmt, 0);
   if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@name");
      const char* str = (const char*)dataBaseInfo.name.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
      if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@version");
      const char* str = (const char*)dataBaseInfo.version.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
      if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@author");
      const char* str = (const char*)dataBaseInfo.author.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
      if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@url");
      const char* str = (const char*)dataBaseInfo.url.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
      if (rc == SQLITE_OK)
   {
      int idx = sqlite3_bind_parameter_index(compiled_stmt, "@epgurl");
      const char* str = (const char*)dataBaseInfo.epgurl.utf8_str();
      rc = sqlite3_bind_text(compiled_stmt, idx, str, -1, SQLITE_TRANSIENT);
   } 
   if (rc == SQLITE_OK)
   {
      rc = sqlite3_step(compiled_stmt);
   }
    
   if (rc != SQLITE_DONE) 
   {
      wxMessageBox("While updating database info data:\n " + wxString(sqlite3_errmsg(m_db),wxConvUTF8), "Error", wxOK | wxICON_STOP, NULL);
   }
        
   sqlite3_finalize(compiled_stmt); 
   return (rc == SQLITE_DONE) ? SQLITE_OK : rc;
            
 
   return rc;
}
  