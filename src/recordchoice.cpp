#include "recordchoice.h"
#include "main.h"
#include "debug.h"

using namespace tvlite;

CRecordChoice::CRecordChoice(wxWindow *parent) : CRecordChoiceBase(parent) 
{
    SetSelection(static_cast<MainFrame *>(m_parent)->GetVLCRecordState());
}

CRecordChoice::~CRecordChoice()
{
}


#ifdef __WXMSW__
void CRecordChoice::OnClickRadioBox( wxCommandEvent& event) 
{
    static_cast<MainFrame *>(m_parent)->SetVLCRecordState(m_recordChoiceBox->GetSelection());
}
#endif

void CRecordChoice::OnLeaveRadioBox( wxFocusEvent& event )
{
    
   static_cast<MainFrame *>(m_parent)->SetVLCRecordState(m_recordChoiceBox->GetSelection());
   static_cast<MainFrame *>(m_parent)->NullifyRecordChoice();
   Destroy();
}

void CRecordChoice::SetSelection(int selection)
{
   m_recordChoiceBox->SetSelection(selection);
}