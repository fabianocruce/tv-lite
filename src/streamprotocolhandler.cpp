#include "streamprotocolhandler.h"
#include "debug.h"

using namespace tvlite;

CStreamProtocolHandler::CStreamProtocolHandler(wxEvtHandler *parent, wxString url, wxArrayString vlcoptions) : 
  CBaseProtocolHandler(parent, url, vlcoptions)
 
{   

}

CStreamProtocolHandler::~CStreamProtocolHandler()
{
   DBG_INFO("SOP Protocol Handler -> delete")
}

void CStreamProtocolHandler::Start()
{
   m_vlcPlayer->Play(m_url, m_vlcoptions);
   
}

void CStreamProtocolHandler::Stop()
{
   m_vlcPlayer->Stop();
   m_stopped = true;
   OnStopAsync();
}
