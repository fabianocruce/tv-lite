#ifndef CVLCOPTDLG_H
#define CVLCOPTDLG_H

#ifdef __WXMSW__
#include "gui-windows.h"
#else
#include "gui.h"
#endif 

class CVLCOptDlg : public CVLCOptDlgBase
{
private:
   CVLCOptDlg();
   CVLCOptDlg(const CVLCOptDlg& rhs);
   CVLCOptDlg& operator=(const CVLCOptDlg& rhs);
   bool ValidateData();

protected:
   virtual void OnPlusClicked( wxCommandEvent& event ); 
   virtual void OnMinusClicked( wxCommandEvent& event ); 
   virtual void OnOkClicked( wxCommandEvent& event );
public:
   CVLCOptDlg(wxWindow *parent);
   virtual ~CVLCOptDlg();
   void GetData( wxArrayString& options );
   void SetData( wxArrayString& options );
   

};

#endif // CVLCOPTDLG_H
