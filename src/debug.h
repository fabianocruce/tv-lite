#ifndef DEBUG_H
#define DEBUG_H

#define DBG(s, ...) printf("%s, line %d, %s(): ", __FILE__, __LINE__, __func__); printf(s __VA_ARGS__); printf("\n");
#define DBG_INFO(...)    DBG("INFO: ", __VA_ARGS__)
#define DBG_WARNING(...) DBG("WARNING: ", __VA_ARGS__)
#define DBG_ERROR(...)   DBG("ERROR: ", __VA_ARGS__)
#endif