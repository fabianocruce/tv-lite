#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: TV-Lite 0.1.0\n"
"POT-Creation-Date: 2021-10-25 12:30+0200\n"
"PO-Revision-Date: 2020-06-06 15:34+0300\n"
"Language-Team: Cristian Burneci\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"X-Poedit-Basepath: ../src\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n==0 || (n!=1 && n%100>=1 && n%100<=19) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _\n"
"Last-Translator: \n"
"Language: ro\n"
"X-Poedit-SearchPath-0: .\n"

#: TVLite.desktop:5 gui-windows.h:151 gui.h:145
msgid "TV-Lite"
msgstr ""

#: TVLite.desktop:8
msgid "TV online viewer"
msgstr ""

#: TVLite.desktop:10
msgid "TVLite"
msgstr ""

#: aceprotocol.cpp:40
msgid ""
"Could not search for the Acestream binary.\n"
" Check your installation!"
msgstr ""

#: aceprotocol.cpp:40 aceprotocol.cpp:47 aceprotocol.cpp:87
#: acestreamprotocolhandler.cpp:85 acestreamprotocolhandler.cpp:183
#: acestreamprotocolhandler.cpp:224 dlprogress.cpp:89 downloadclient.cpp:52
#: main.cpp:336 main.cpp:342 main.cpp:358 main.cpp:448 main.cpp:1543
#: main.cpp:1670 sopprotocol.cpp:86 sopprotocol.cpp:92 sopprotocol.cpp:108
#: subscription.cpp:257 subscription.cpp:265 subscription.cpp:328
#: subscriptionlistdialog.cpp:39 subscriptionlistdialog.cpp:45
#: subscriptionlistdialog.cpp:78 subscriptionlistdialog.cpp:104
#: subscriptionlistdialog.cpp:186 subscriptionlistdialog.cpp:192
msgid "Error"
msgstr ""

#: aceprotocol.cpp:47
msgid "Could not find the Acestream engine binary in the path."
msgstr ""

#: aceprotocol.cpp:87
msgid "Could not execute Acestream engine!"
msgstr ""

#: acestreamprotocolhandler.cpp:80
msgid "Requesting data from Acestream"
msgstr ""

#: acestreamprotocolhandler.cpp:85 acestreamprotocolhandler.cpp:224
msgid "Error retrieving data from Acestream engine"
msgstr ""

#: acestreamprotocolhandler.cpp:86 main.cpp:211 main.cpp:499
msgid "Ready"
msgstr ""

#: acestreamprotocolhandler.cpp:183
msgid "Error response from Acestream engine\n"
msgstr ""

#: channel.cpp:310
msgid "Unknown protocol"
msgstr ""

#: channel.cpp:318
msgid "Sopcast address"
msgstr ""

#: channel.cpp:326
msgid "Web address"
msgstr ""

#: channel.cpp:330
msgid "File "
msgstr ""

#: channel.cpp:334
msgid "Acestream id"
msgstr ""

#: channel.cpp:338
msgid "Unknown address"
msgstr ""

#: channeledit.cpp:12
msgid "Addresses"
msgstr ""

#: channeledit.cpp:13 gui-windows.cpp:548 gui-windows.cpp:608 gui.cpp:564
#: gui.cpp:624 subscriptionlistdialog.cpp:15
msgid "Name"
msgstr ""

#: channeledit.cpp:72 localdialog.cpp:35
msgid "Please input a valid name"
msgstr ""

#: channeledit.cpp:72 channeledit.cpp:98 channeledit.cpp:102 localdialog.cpp:35
#: prefdialog.cpp:105 prefdialog.cpp:112 prefdialog.cpp:131 prefdialog.cpp:139
#: prefdialog.cpp:147 prefdialog.cpp:157 vlcoptdlg.cpp:60
msgid "Validation error"
msgstr ""

#: channeledit.cpp:82 channeledit.cpp:118
msgid "Click here to input the address..."
msgstr ""

#: channeledit.cpp:98
msgid "Empty address detected "
msgstr ""

#: channeledit.cpp:102
msgid "Please, enter at least an address"
msgstr ""

#: channeledit.cpp:128
msgid "Do you really wish to delete the selected address?"
msgstr ""

#: channeledit.cpp:129 locallistdialog.cpp:92 main.cpp:1354
#: subscriptionlistdialog.cpp:129 vlcoptdlg.cpp:80
msgid "Question"
msgstr ""

#: channelinfo.cpp:24
msgid "Channel name: "
msgstr ""

#: channelinfo.cpp:25
msgid "Channel group: "
msgstr ""

#: dlprogress.cpp:89
msgid "Could not update subscription "
msgstr ""

#: downloadclient.cpp:52
msgid "Error downloading channel list"
msgstr ""

#: gui-windows.cpp:26 gui.cpp:21
msgid "Manage &Lists..."
msgstr ""

#: gui-windows.cpp:28 gui.cpp:23
msgid "Add Local List"
msgstr ""

#: gui-windows.cpp:34 gui.cpp:29
msgid "Save Whole Channel List As..."
msgstr ""

#: gui-windows.cpp:38 gui.cpp:33
msgid "Save Current View As..."
msgstr ""

#: gui-windows.cpp:44 gui.cpp:39
msgid "Save Subscription As Local List"
msgstr ""

#: gui-windows.cpp:49 gui.cpp:44
msgid "Manage Local Lists"
msgstr ""

#: gui-windows.cpp:52 gui.cpp:47
msgid "Manage Subscriptions"
msgstr ""

#: gui-windows.cpp:60 gui.cpp:55
msgid "Preferences"
msgstr ""

#: gui-windows.cpp:66 gui.cpp:61
msgid "E&xit"
msgstr ""

#: gui-windows.cpp:69 gui.cpp:64
msgid "&TVLite"
msgstr ""

#: gui-windows.cpp:73 gui.cpp:68
msgid "Full Screem"
msgstr ""

#: gui-windows.cpp:76 gui.cpp:71
msgid "Channel List"
msgstr ""

#: gui-windows.cpp:83 gui.cpp:78
msgid "Aspect Ratio"
msgstr ""

#: gui-windows.cpp:85 gui.cpp:80
msgid "Default"
msgstr ""

#: gui-windows.cpp:89 gui.cpp:84
msgid "16:9"
msgstr ""

#: gui-windows.cpp:93 gui.cpp:88
msgid "4:3"
msgstr ""

#: gui-windows.cpp:101 gui.cpp:96
msgid "Video Tracks"
msgstr ""

#: gui-windows.cpp:105 gui.cpp:100
msgid "Audio Tracks"
msgstr ""

#: gui-windows.cpp:109 gui.cpp:104
msgid "Subtitle Tracks"
msgstr ""

#: gui-windows.cpp:112 gui.cpp:107
msgid "&View"
msgstr ""

#: gui-windows.cpp:116 gui.cpp:111
msgid "About"
msgstr ""

#: gui-windows.cpp:119 gui.cpp:114
msgid "Help"
msgstr ""

#: gui-windows.cpp:133 gui-windows.h:191 gui.cpp:128 gui.h:229
msgid "Local Lists"
msgstr ""

#: gui-windows.cpp:133 gui-windows.h:222 gui.cpp:128 gui.h:260
msgid "Subscriptions"
msgstr ""

#: gui-windows.cpp:135 gui.cpp:130
msgid "List Types"
msgstr ""

#: gui-windows.cpp:139 gui.cpp:134
msgid "Channel List:"
msgstr ""

#: gui-windows.cpp:178 gui.cpp:173
msgid "Total:"
msgstr ""

#: gui-windows.cpp:202 gui.cpp:197
msgid "Asc"
msgstr ""

#: gui-windows.cpp:202 gui.cpp:197
msgid "Desc"
msgstr ""

#: gui-windows.cpp:202 gui-windows.cpp:712 gui-windows.cpp:785 gui.cpp:197
#: gui.cpp:406 gui.cpp:897
msgid "None"
msgstr ""

#: gui-windows.cpp:204 gui.cpp:199
msgid "Order"
msgstr ""

#: gui-windows.cpp:432 gui-windows.cpp:488 gui.cpp:448
msgid "New"
msgstr ""

#: gui-windows.cpp:435 gui-windows.cpp:500 gui.cpp:451 gui.cpp:516
msgid "Edit"
msgstr ""

#: gui-windows.cpp:438 gui-windows.cpp:503 gui.cpp:454 gui.cpp:519
msgid "Delete"
msgstr ""

#: gui-windows.cpp:447 gui-windows.cpp:512 gui.cpp:463 gui.cpp:528
msgid "Close"
msgstr ""

#: gui-windows.cpp:491 gui.cpp:507
msgid "New Xtream"
msgstr ""

#: gui-windows.cpp:497 gui.cpp:513 subscriptionlistdialog.cpp:49
#: subscriptionlistdialog.cpp:82 subscriptionlistdialog.cpp:108
#: subscriptionlistdialog.cpp:196
msgid "Update"
msgstr ""

#: gui-windows.cpp:555 gui-windows.cpp:622 gui.cpp:571 gui.cpp:638
#: subscriptionlistdialog.cpp:16
msgid "URL"
msgstr ""

#: gui-windows.cpp:565 gui.cpp:581
msgid " ... "
msgstr ""

#: gui-windows.cpp:615 gui.cpp:631
msgid "Author"
msgstr ""

#: gui-windows.cpp:629 gui.cpp:645
msgid "Version"
msgstr ""

#: gui-windows.cpp:712 gui.cpp:406
msgid "Record"
msgstr ""

#: gui-windows.cpp:712 gui.cpp:406
msgid "Stream"
msgstr ""

#: gui-windows.cpp:714 gui.cpp:408
msgid "Recording Options"
msgstr ""

#: gui-windows.cpp:750 gui.cpp:862
msgid "Streaming port"
msgstr ""

#: gui-windows.cpp:760 gui.cpp:872
msgid "Recording path"
msgstr ""

#: gui-windows.cpp:770 gui-windows.cpp:823 gui.cpp:882 gui.cpp:935
msgid "..."
msgstr ""

#: gui-windows.cpp:780 gui.cpp:892
msgid "General"
msgstr ""

#: gui-windows.cpp:785 gui.cpp:897
msgid "Any"
msgstr ""

#: gui-windows.cpp:785 gui.cpp:897
msgid "Custom"
msgstr ""

#: gui-windows.cpp:787 gui.cpp:899
msgid "Hardware acceleration type"
msgstr ""

#: gui-windows.cpp:794 gui.cpp:906
msgid "Custom HW acceleration string"
msgstr ""

#: gui-windows.cpp:808 gui.cpp:920
msgid "VLC"
msgstr ""

#: gui-windows.cpp:813 gui.cpp:925
msgid "Acestream executable path"
msgstr ""

#: gui-windows.cpp:832 gui.cpp:950
msgid "Maximum disk cache size"
msgstr ""

#: gui-windows.cpp:839 gui.cpp:957
msgid "MB"
msgstr ""

#: gui-windows.cpp:846 gui.cpp:964
msgid "The settings on this page will only be effective after program restart!"
msgstr ""

#: gui-windows.cpp:854 gui.cpp:972
msgid "Acestream"
msgstr ""

#: gui-windows.cpp:859 gui.cpp:977
msgid "Use Proxy"
msgstr ""

#: gui-windows.cpp:867 gui.cpp:985
msgid "Type"
msgstr ""

#: gui-windows.cpp:871 gui.cpp:989
msgid "Address"
msgstr ""

#: gui-windows.cpp:875 gui.cpp:993
msgid "Port"
msgstr ""

#: gui-windows.cpp:879 gui.cpp:997
msgid "HTTP"
msgstr ""

#: gui-windows.cpp:879 gui.cpp:997
msgid "SOCKS"
msgstr ""

#: gui-windows.cpp:894 gui.cpp:1012
msgid "Use Authentication"
msgstr ""

#: gui-windows.cpp:902 gui-windows.cpp:984 gui.cpp:1020 gui.cpp:1102
msgid "Username"
msgstr ""

#: gui-windows.cpp:909 gui-windows.cpp:991 gui.cpp:1027 gui.cpp:1109
msgid "Password"
msgstr ""

#: gui-windows.cpp:923
msgid "Proxy"
msgstr ""

#: gui-windows.cpp:970 gui.cpp:1088
msgid "Subscription Name"
msgstr ""

#: gui-windows.cpp:977 gui.cpp:1095
msgid "Host and port (ex. http://host.xyz:port)"
msgstr ""

#: gui-windows.cpp:998 gui.cpp:1116
msgid "m3u_plus"
msgstr ""

#: gui-windows.cpp:998 gui.cpp:1116
msgid "m3u"
msgstr ""

#: gui-windows.cpp:1000 gui.cpp:1118
msgid "Playlist Type"
msgstr ""

#: gui-windows.cpp:1004 gui.cpp:1122
msgid "mpeg-ts"
msgstr ""

#: gui-windows.cpp:1004 gui.cpp:1122
msgid "hls"
msgstr ""

#: gui-windows.cpp:1004 gui.cpp:1122
msgid "rtmp"
msgstr ""

#: gui-windows.cpp:1006 gui.cpp:1124
msgid "Output Type"
msgstr ""

#: gui-windows.cpp:1096 gui.cpp:686 main.cpp:169
msgid "Channel Name"
msgstr ""

#: gui-windows.cpp:1103 gui.cpp:693
msgid "Group"
msgstr ""

#: gui-windows.cpp:1110 gui.cpp:700
msgid "Channel Addresses"
msgstr ""

#: gui-windows.h:252 gui.h:290
msgid "Path"
msgstr ""

#: gui-windows.h:283 gui.h:321
msgid "Edit Local List"
msgstr ""

#: gui-windows.h:389 gui.h:449
msgid "TV-Lite - Preferences"
msgstr ""

#: gui-windows.h:441 gui.h:501
msgid "Channel Information"
msgstr ""

#: gui-windows.h:506 gui.h:362
msgid "Channel Editor"
msgstr ""

#: gui-windows.h:535 gui.h:529
msgid "VLC options"
msgstr ""

#: gui.cpp:504
msgid "New URL"
msgstr ""

#: gui.cpp:941
msgid "Disk"
msgstr ""

#: gui.cpp:941
msgid "Memory"
msgstr ""

#: gui.cpp:943
msgid "Cache Type"
msgstr ""

#: gui.cpp:1041
msgid "Network"
msgstr ""

#: locallist.cpp:29
msgid "Loading local list"
msgstr ""

#: locallistdialog.cpp:91
msgid "Do you really wish to delete the selected list, "
msgstr ""

#: main.cpp:127
msgid "Could not initialize program configuration"
msgstr ""

#: main.cpp:170
msgid "Groups"
msgstr ""

#: main.cpp:336
msgid "No Item selected"
msgstr ""

#: main.cpp:342 main.cpp:358
msgid "This item has no media sources"
msgstr ""

#: main.cpp:448
msgid "Acestream engine not found"
msgstr ""

#: main.cpp:767
msgid "Total: "
msgstr ""

#: main.cpp:767
msgid " channels and "
msgstr ""

#: main.cpp:767
msgid " sources."
msgstr ""

#: main.cpp:794
msgid "Finding groups..."
msgstr ""

#: main.cpp:853
msgid "<All Channels>"
msgstr ""

#: main.cpp:901
msgid "Update channelList"
msgstr ""

#: main.cpp:1075 main.cpp:1122 main.cpp:1146
msgid "New Item"
msgstr ""

#: main.cpp:1147
msgid "Edit Item"
msgstr ""

#: main.cpp:1148
msgid "Delete Item"
msgstr ""

#: main.cpp:1153
msgid "Information"
msgstr ""

#: main.cpp:1353
msgid "Do you really wish to delete the selected channel?"
msgstr ""

#: main.cpp:1480
msgid "The list was saved."
msgstr ""

#: main.cpp:1532
msgid "Save File As _?"
msgstr ""

#: main.cpp:1533
msgid "TV-Maxe lists (*.db)|*.db|All files (*)|*"
msgstr ""

#: main.cpp:1543
msgid "Error while trying to write file: "
msgstr ""

#: main.cpp:1551
msgid "The list has been saved."
msgstr ""

#: main.cpp:1551
msgid "Save list"
msgstr ""

#: main.cpp:1560
msgid ""
"Viewer for online TV Channels - TV-maxe like\n"
"Version "
msgstr ""

#: main.cpp:1594
msgid "Buffering"
msgstr ""

#: main.cpp:1635
msgid "Playing"
msgstr ""

#: main.cpp:1649
msgid "Paused"
msgstr ""

#: main.cpp:1652
msgid "Stopped"
msgstr ""

#: main.cpp:1670
msgid "The player encountered an error while trying to play URL"
msgstr ""

#: prefdialog.cpp:105
msgid "Please input a valid recording path"
msgstr ""

#: prefdialog.cpp:112
msgid "Please provide HW acceleration type"
msgstr ""

#: prefdialog.cpp:131 prefdialog.cpp:139
msgid "Please input a valid proxy port value"
msgstr ""

#: prefdialog.cpp:147
msgid "Please input a proxy address"
msgstr ""

#: prefdialog.cpp:157
msgid "Please input a proxy username"
msgstr ""

#: prefdialog.cpp:177
msgid "Choose Recording Folder"
msgstr ""

#: prefdialog.cpp:206
msgid "Choose Acestream file"
msgstr ""

#: sopprotocol.cpp:86
msgid ""
"Could not search for the Sopcast binary.\n"
" Check your installation!"
msgstr ""

#: sopprotocol.cpp:92
msgid "Could not find the Sopcast binary in the path."
msgstr ""

#: sopprotocol.cpp:108
msgid "Could not execute Sopcast!"
msgstr ""

#: sopprotocolhandler.cpp:40
msgid "Starting Sopcast"
msgstr ""

#: subscription.cpp:206
msgid "Parsing playlist"
msgstr ""

#: subscription.cpp:257
msgid "Malformed M3U list!"
msgstr ""

#: subscription.cpp:265
msgid "Format not recognized"
msgstr ""

#: subscription.cpp:328
msgid "Could not open playlist"
msgstr ""

#: subscription.cpp:519
msgid "Saving..."
msgstr ""

#: subscriptiondialog.cpp:70
msgid "Open XYZ file"
msgstr ""

#: subscriptionlistdialog.cpp:39 subscriptionlistdialog.cpp:186
msgid "Found another subscription with the same URL"
msgstr ""

#: subscriptionlistdialog.cpp:45 subscriptionlistdialog.cpp:192
msgid "Could not set up the selected subscription"
msgstr ""

#: subscriptionlistdialog.cpp:49 subscriptionlistdialog.cpp:196
msgid "Subscription set up"
msgstr ""

#: subscriptionlistdialog.cpp:78
msgid "Could not modify the selected subscription"
msgstr ""

#: subscriptionlistdialog.cpp:82
msgid "Subscription modified"
msgstr ""

#: subscriptionlistdialog.cpp:104
msgid "Could not update the selected subscription"
msgstr ""

#: subscriptionlistdialog.cpp:108
msgid "Subscription Updated"
msgstr ""

#: subscriptionlistdialog.cpp:128
msgid "Do you really wish to delete the selected subscription, "
msgstr ""

#: vlcoptdlg.cpp:9
msgid "Option"
msgstr ""

#: vlcoptdlg.cpp:23 vlcoptdlg.cpp:49 vlcoptdlg.cpp:69
msgid "Click here to add option..."
msgstr ""

#: vlcoptdlg.cpp:60
msgid "Invalid option (blank)"
msgstr ""

#: vlcoptdlg.cpp:79
msgid "Do you really wish to delete the selected option?"
msgstr ""
