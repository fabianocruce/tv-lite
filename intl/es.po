msgid ""
msgstr ""
"Project-Id-Version: TV-Lite 0.1.0\n"
"POT-Creation-Date: 2020-06-06 17:00+0300\n"
"PO-Revision-Date: 2020-06-16 18:01+0300\n"
"Last-Translator: \n"
"Language-Team: Luta Dumitru\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"X-Poedit-Basepath: ../src\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n==0 || (n!=1 && n%100>=1 && n"
"%100<=19) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _\n"
"X-Poedit-SearchPath-0: .\n"

#: TVLite.desktop:5 gui.h:107
msgid "TV-Lite"
msgstr ""

#: TVLite.desktop:8
msgid "TV online viewer"
msgstr "Visor de canales de TV en línea"

#: aceprotocol.cpp:34
msgid ""
"Could not search for the Acestream binary.\n"
" Check your installation!"
msgstr ""
"No encuentro el programa Acestream.\n"
"Comprueba tu instalación!"

#: aceprotocol.cpp:34 aceprotocol.cpp:41 aceprotocol.cpp:58
#: acestreamprotocolhandler.cpp:63 acestreamprotocolhandler.cpp:137
#: main.cpp:277 main.cpp:895 sopprotocol.cpp:86 sopprotocol.cpp:92
#: sopprotocol.cpp:108 subscription.cpp:107 subscription.cpp:153
#: subscriptiondialog.cpp:58 subscriptionlistdialog.cpp:57
msgid "Error"
msgstr "Error"

#: aceprotocol.cpp:41
msgid "Could not find the Acestream engine binary in the path."
msgstr "No se pudo encontrar el binario Acestream en la ruta."

#: aceprotocol.cpp:58
msgid "Could not execute Acestream engine!"
msgstr "No se pudo ejecutar el programa Acestream!"

#: acestreamprotocolhandler.cpp:63 acestreamprotocolhandler.cpp:137
msgid "Error retrieving data from Acestream engine"
msgstr "Error al recuperar datos del programa Acestream"

#: channeledit.cpp:48 localdialog.cpp:34
msgid "Please input a valid name"
msgstr "Por favor ingrese un nombre válido"

#: channeledit.cpp:48 channeledit.cpp:62 localdialog.cpp:34
msgid "Validation error"
msgstr "Error de validacion"

#: channeledit.cpp:62
msgid "Please input at least a valid address"
msgstr "Ingrese al menos una dirección válida"

#: gui.cpp:21
msgid "Manage &Lists..."
msgstr "Administrar &Listas..."

#: gui.cpp:23
msgid "Add Local List"
msgstr "Agregar lista local"

#: gui.cpp:29
msgid "Save Local List As..."
msgstr "Guardar lista local como..."

#: gui.cpp:33
msgid "Save Subscription As Local List"
msgstr "Guardar suscripción como lista local"

#: gui.cpp:38
msgid "Manage Local Lists"
msgstr "Administrar listas locales"

#: gui.cpp:41
msgid "Manage Subscriptions"
msgstr "Administrar Suscripciones"

#: gui.cpp:47
msgid "&Options"
msgstr "&Opciones"

#: gui.cpp:53
msgid "E&xit"
msgstr ""

#: gui.cpp:56
msgid "&TVLite"
msgstr ""

#: gui.cpp:60
msgid "Full Screem"
msgstr "Pantalla Completa"

#: gui.cpp:63
msgid "Channel List"
msgstr "Lista de canales"

#: gui.cpp:70
msgid "Aspect Ratio"
msgstr "Relación de aspecto"

#: gui.cpp:72
msgid "Default"
msgstr "Por defecto"

#: gui.cpp:76
msgid "16:9"
msgstr ""

#: gui.cpp:80
msgid "4:3"
msgstr ""

#: gui.cpp:85
msgid "&View"
msgstr "&Ver"

#: gui.cpp:89
msgid "About"
msgstr "Acerca de"

#: gui.cpp:92
msgid "Help"
msgstr "Ayuda"

#: gui.cpp:106 gui.h:141
msgid "Local Lists"
msgstr "Listas locales"

#: gui.cpp:106 gui.h:170
msgid "Subscriptions"
msgstr "Suscripciones"

#: gui.cpp:108
msgid "List Types"
msgstr "Tipos de lista"

#: gui.cpp:112
msgid "Channel List:"
msgstr "Lista de canale:"

#: gui.cpp:124
msgid "Total:"
msgstr "Total:"

#: gui.cpp:257 gui.cpp:313
msgid "New"
msgstr "Nuevo"

#: gui.cpp:260 gui.cpp:322
msgid "Edit"
msgstr "Editar"

#: gui.cpp:263 gui.cpp:325
msgid "Delete"
msgstr "Eliminar"

#: gui.cpp:272 gui.cpp:334
msgid "Close"
msgstr "Cerrar"

#: gui.cpp:319 subscriptionlistdialog.cpp:68
msgid "Update"
msgstr "Actualizar"

#: gui.cpp:368 gui.cpp:428 subscriptionlistdialog.cpp:9
msgid "Name"
msgstr "Nombre"

#: gui.cpp:375 gui.cpp:442 subscriptionlistdialog.cpp:10
msgid "URL"
msgstr "URL"

#: gui.cpp:385
msgid " ... "
msgstr ""

#: gui.cpp:435
msgid "Author"
msgstr "Autor"

#: gui.cpp:449
msgid "Version"
msgstr "Versión"

#: gui.cpp:490
msgid "Channel Name"
msgstr "Nombre del Canal"

#: gui.cpp:497
msgid "Channel Addresses"
msgstr "Direcciones de canales"

#: gui.h:200
msgid "Path"
msgstr "Ruta"

#: gui.h:231
msgid "Edit Local List"
msgstr "Editar lista local"

#: locallistdialog.cpp:73 subscriptionlistdialog.cpp:81
msgid "Do you really wish to delete the selected subscription, "
msgstr "¿Realmente desea eliminar la suscripción seleccionada? "

#: locallistdialog.cpp:74 subscriptionlistdialog.cpp:82
msgid "Question"
msgstr "Pregunta"

#: main.cpp:81
msgid "Could not initialize program configuration"
msgstr "No se pudo inicializar la configuración del programa"

#: main.cpp:111 main.cpp:316
msgid "Ready"
msgstr "Listo"

#: main.cpp:277
msgid "Acestream engine not found"
msgstr "Acestream no encontrado"

#: main.cpp:466
msgid "Total: "
msgstr "Total: "

#: main.cpp:466
msgid " channels and "
msgstr " canales y "

#: main.cpp:466
msgid " sources."
msgstr " fuentes."

#: main.cpp:548 main.cpp:568
msgid "New Item"
msgstr "Nuevo artículo"

#: main.cpp:569
msgid "Edit Item"
msgstr "Editar artículo"

#: main.cpp:570
msgid "Delete Item"
msgstr "Eliminar elemento"

#: main.cpp:807
msgid "The list was saved."
msgstr "La lista fue guardada."

#: main.cpp:826
msgid "Save File As _?"
msgstr "Guardar archivo como...?"

#: main.cpp:827
msgid "TV-Maxe lists (*.db)|*.db|All files (*)|*"
msgstr ""

#: main.cpp:853
msgid "Viewer for online TV Channels - TV-maxe like"
msgstr "Visor de canales de TV en línea - TV-maxe like"

#: main.cpp:878
msgid "Buffering"
msgstr ""

#: main.cpp:883
msgid "Playing"
msgstr "Reproducción"

#: main.cpp:887
msgid "Paused"
msgstr "Pausado"

#: main.cpp:890
msgid "Stopped"
msgstr "Detenido"

#: main.cpp:895
msgid "The player encountered an error while trying to play URL"
msgstr "Error al representar la dirección"

#: sopprotocol.cpp:86
msgid ""
"Could not search for the Sopcast binary.\n"
" Check your installation!"
msgstr ""
"No se pudo buscar el binario de Sopcast.\n"
"Comprueba tu instalación!"

#: sopprotocol.cpp:92
msgid "Could not find the Sopcast binary in the path."
msgstr "No se pudo encontrar el binario Sopcast en la ruta."

#: sopprotocol.cpp:108
msgid "Could not execute Sopcast!"
msgstr "No se pudo ejecutar Sopcast!"

#: sopprotocolhandler.cpp:40
msgid "Starting Sopcast"
msgstr "Iniciando Sopcast"

#: subscription.cpp:107
msgid "Error downloading channel list"
msgstr "Error al descargar la lista de canales"

#: subscription.cpp:153
msgid "Format not recognized"
msgstr "Formato no reconocido"

#: subscriptiondialog.cpp:58
msgid "Found another subscription with the same URL!"
msgstr "Encontré otra suscripción con la misma dirección!"

#: subscriptiondialog.cpp:85
msgid "Open XYZ file"
msgstr "Abrir archivo XYZ"

#: subscriptionlistdialog.cpp:57
msgid "Could not update the selected subscription"
msgstr "No se pudo actualizar la suscripción seleccionada"

#: subscriptionlistdialog.cpp:68
msgid "Subscription Updated"
msgstr "Suscripción actualizada"
